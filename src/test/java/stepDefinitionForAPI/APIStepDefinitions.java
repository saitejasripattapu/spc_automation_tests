package stepDefinitionForAPI;



import com.fasterxml.jackson.annotation.JsonFormat;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import utils.*;
import pojo.*;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import static io.restassured.RestAssured.*;
import static org.testng.AssertJUnit.assertEquals;


public class APIStepDefinitions extends Utils_API {

        Properties prop=new Properties();


        RequestSpecification res;
        ResponseSpecification resspec;
        Response response;
//       Hooks hooks=new Hooks();
       long petId;
       long orderId;

    public static final JsonFormat.Feature ACCEPT_SINGLE_VALUE_AS_ARRAY = null;




    payLoad payLoad=new payLoad();



    @When("User calls {string} API with {string} http request")
        public void api_with_http_request(String resource, String httpMethod) throws IOException {
                FileInputStream fis=new FileInputStream(System.getProperty("user.dir")+"//src//test//resources//cucumber.properties");
                prop.load(fis);
                RestAssured.baseURI = prop.getProperty("baseURL");
                APIList apiList = APIList.valueOf(resource);
                if(httpMethod.equalsIgnoreCase("POST")) {
                    response=res.when().post(apiList.getResource());
                }
                else if(httpMethod.equalsIgnoreCase("GET"))
                {
                    response=res.when().get(apiList.getResource());
                }

        }



    @Given("the request payload for addPet with petID {string}")
    public void theRequestPayloadForAddPetWithPetID(String petID) throws IOException {
        petId = Long.parseLong(petID);
        res=given().spec(requestSpecification()).header("Content-Type", "application/json").
                header("accept","application/json").body(payLoad.addPet(petId));
    }

        @Then("verify the pet is added successfully and get the petID")
        public void verify_the_pet_is_added_successfully_and_get_the_petID() {
        assertEquals(response.getStatusCode(),200);
                System.out.println("returned petID->"+getJsonPath(response,"id"));
            Assert.assertEquals(petId,Long.parseLong(getJsonPath(response,"id")));
        }






    @Given("the request payload for getPet")
    public void theRequestPayloadForGetPet() throws IOException {
        res=given().spec(requestSpecification()).header("accept", "application/json");

    }

    @Given("the request headers for storeInventory")
    public void theRequestHeadersForStoreInventory() throws IOException {
        res=given().spec(requestSpecification()).header("accept", "application/json")
                .header("api_key","4e9e83c8-d015-4b93-95a0-4d507b38496f");
    }


    @Then("verify the data successfully and get the available count")
    public void verifyTheDataSuccessfullyAndGetTheAvailableCount() {
        assertEquals(response.getStatusCode(),200);
        String availableCount= getJsonPath(response,"available");
        System.out.println(availableCount);

    }

    @Given("the query param  for status")
    public void theQueryParamForStatus() throws IOException {
        res=given().spec(requestSpecification()).queryParam("status","available")
                .header("accept", "application/json");
           }

    @Then("verify the data successfully and get the count of avaible pets")
    public void verifyTheDataSuccessfullyAndGetTheCountOfAvaiblePets() {
        System.out.println(response.getStatusCode());
        assertEquals(response.getStatusCode(),200);
       List<PetModel> pets = Arrays.asList(response.getBody().as(PetModel[].class));
      System.out.println("converted list");
      System.out.println("size->"+pets.size());


    }



    @Then("verify the data successfully and get the orderID")
    public void verifyTheDataSuccessfullyAndGetTheOrderID() {
        assertEquals(response.getStatusCode(),200);
        String orderID= getJsonPath(response,"id");
        System.out.println("Order ID Is "+ orderID);

    }

    @Given("the request body and the path of the image")
    public void theRequestBodyAndThePathOfTheImage() throws IOException {
        File file= new File("./src/test/resources/dogImage.jfif");

        res=given().spec(requestSpecification()).
                header("accept", "application/json").header("Content-Type", "multipart/form-data;boundary=----webkitformboundary2fvqbgcbynvtvptx").
                multiPart("file",file,"multipart/form-data").
                 formParam("type","image/jpeg");
    }

      @Then("verify the image is added successfully")
    public void verifyTheImageIsAddedSuccessfully() {
          assertEquals(response.getStatusCode(),200);
          String message= getJsonPath(response,"message");
          System.out.println("Message Is "+ message);
          System.out.println(response.prettyPrint());

    }


    @When("User calls uploadImage API with pet id {string}")
    public void userCallsUploadImageAPIWithPetId(String arg0) {
        response=res.when().post("/pet/"+arg0+"/uploadImage");
    }

    @Given("the request payload for updatePet")
    public void theRequestPayloadForUpdatePet() throws IOException {
        res=given().spec(requestSpecification()).
                header("accept", "application/json").header("Content-Type", "application/json").body(payLoad.addPet(petId));
    }

    @When("User calls updatePet API with PUT httpmethod")
    public void userCallsUpdatePetAPIWithPUTHttpmethod() {
        response=res.when().put("/pet");
    }

    @Then("verify the data is updated successfully")
    public void verifyTheDataIsUpdatedSuccessfully() {
        assertEquals(response.getStatusCode(),200);
        String dogName= getJsonPath(response,"name");
        System.out.println("Message Is: "+ dogName);
    }

    @Given("the request payload for findpet")
    public void theRequestPayloadForFindpet() throws IOException {
        res=given().spec(requestSpecification()).
                header("accept", "application/json");
    }


    @Then("verify the data is fetched successfully")
    public void verifyTheDataIsFetchedSuccessfully() {
        assertEquals(response.getStatusCode(),200);


        System.out.println(response.getBody().prettyPrint());
        PetModel petObj = response.getBody().as(PetModel.class);
        System.out.println("converted pet Obj");
        System.out.println(petObj);

//        List<PetModel> pets = Arrays.asList(response.getBody().as(PetModel[].class));
////        System.out.println("converted list");
////        System.out.println(pets);

    }

    @When("User calls findPet By {int} API with GET httpmethod")
    public void userCallsFindPetByAPIWithGETHttpmethod(int petId) {
        response= res.when().get("/pet/"+petId);
    }


    @Given("the payload and request headers and orderID as {string}")
    public void thePayloadAndRequestHeadersAndOrderIDAs(String orderID) throws IOException {
        orderId=Long.parseLong(orderID);
        res=given().spec(requestSpecification()).
                header("accept", "application/json").header("Content-Type", "application/json").body(payLoad.placeOrder(orderId));
    }

    @Given("the request payload for findOrder")
    public void theRequestPayloadForFindOrder() throws IOException {
        res=given().spec(requestSpecification()).
                header("accept", "application/json");
    }

    @When("User calls findOrder By {int} API with GET httpmethod")
    public void userCallsFindOrderByAPIWithGETHttpmethod(int orderId) {
        response= res.when().get("/store/order/"+orderId);
    }

    @Then("verify the order is fetched successfully")
    public void verifyTheOrderIsFetchedSuccessfully() {
        assertEquals(response.getStatusCode(),200);
        System.out.println(response.getBody().prettyPrint());
        OrderModel orderObj = response.getBody().as(OrderModel.class);
        System.out.println("converted Order Obj");
        System.out.println(orderObj);
    }
}

