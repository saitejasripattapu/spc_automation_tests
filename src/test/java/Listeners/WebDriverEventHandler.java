package Listeners;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.WebDriverListener;


public class WebDriverEventHandler implements WebDriverListener {
    @Override
    public void beforeClick(WebElement element) {
        try {
            Thread.sleep(2000);
            System.out.println("Before Clicking");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
