package cucumberOptions;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/java/features/WebTests.feature",glue ="stepDefinitionForWeb"
        ,monochrome=true,
        tags = "@Regression",
        plugin= {"html:target/cucumber.html", "json:target/cucumber.json",
                "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:${version}"})

public class TestNG_Web_Runner
        extends AbstractTestNGCucumberTests
{
}
