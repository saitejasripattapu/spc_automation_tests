Feature: Automating API Swagger Petstore

  @Regression @API
  Scenario: Add a new pet to the store
    Given the request payload for addPet with petID "34567"
    When User calls "addPet" API with "POST" http request
    Then verify the pet is added successfully and get the petID

  @Regression @API
  Scenario: Find Pet By Status
    Given the query param  for status
    When User calls "findByStatus" API with "GET" http request
    Then verify the data successfully and get the count of avaible pets

  @Regression @API
  Scenario: Upload pet Image
    Given the request body and the path of the image
    When User calls uploadImage API with pet id "678"
    Then verify the image is added successfully

  @Regression @API
  Scenario: Update an existing pet
    Given the request payload for updatePet
    When User calls updatePet API with PUT httpmethod
    Then verify the data is updated successfully

  @Regression @API
  Scenario: Update an existing pet
    Given the request payload for updatePet
    When User calls updatePet API with PUT httpmethod
    Then verify the data is updated successfully

  @Regression @API
  Scenario: find pet by ID
    Given the request payload for findpet
    When User calls findPet By 34567 API with GET httpmethod
    Then verify the data is fetched successfully


  @Regression @API
  Scenario: Get Store Inventory
    Given the request headers for storeInventory
    When User calls "getStoreInventory" API with "GET" http request
    Then verify the data successfully and get the available count

  @Regression @API
  Scenario: place an order for Purchasing a pet
    Given the payload and request headers and orderID as "12245"
    When User calls "purchasePet" API with "POST" http request
    Then verify the data successfully and get the orderID

  @Regression @API
  Scenario: find order by orderID
    Given the request payload for findOrder
    When User calls findOrder By 12245 API with GET httpmethod
    Then verify the order is fetched successfully

