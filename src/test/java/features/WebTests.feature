Feature: Automating UI Testing playground

@Regression @Testcase1
  Scenario: Opening the page
    When user calls UI Testing playgroundpage
    Then the user should successfully navigate to Home Page
    And validate that various tabs exists

@Regression @TestCase2
  Scenario: Button with DynamicID
  When user clicks on DynamicID link
  Then the User will navigate to Description
  And user will locate the button and will click on it

@Regression @TestCase3
  Scenario: Class Attribute Identification
    When user clicks on ClassAttribute link
    Then the User will navigate to Description page
    And user will locate the green button and record click
    Then verify that the button cannot be hit twice

@TestCase4
  Scenario: Hidden Layers
    When user clicks on Hiddenlayers link
    Then the User will navigate to hiddenlayers Description page
    And user will locate the blue button and will click on it
    Then user will click on OK in the pop-up

@Regression @TestCase5
  Scenario: Load Delay
    When user clicks on Load Delay link
    Then the User will navigate to Description page after 15 seconds loading
    And Verify the button after the page load

@Regression @TestCase6
  Scenario: Ajax Data
    When user clicks on Ajax Data  link
    Then the User will navigate to Ajax Data Description page
    And click on the button and wait for 15 seconds
    Then verify the text of the loaded label



@Regression @TestCase7
  Scenario: Mouse Click
    When user clicks on click link
    Then the User will navigate to mouse click Description page
    And click on the blue button using mouse event
    Then verify the button turns Green color


 @Regression @TestCase8
  Scenario: Keyboard Input
    When user clicks on Text Input link
    Then the User will navigate to keyboard Description page
    And enter the "text" in the text field and click on the button
    Then verify the button texts changes to the text entered by you.

  @Regression @TestCase9
  Scenario: Scroll View
    When user clicks on Scroll bars link
    Then the User will navigate to scroll bars Description page
    And scroll vertically and horizontally until the button is visible.
    Then verify the button text is visible.
#

  @Regression @TestCase10
  Scenario: Dynamic Table
    When user clicks on Dynamic Table link
    Then the User will navigate to dynamic table Description page
    And Get the value of CPU for chrome process.
    Then compare it with the value in the yellow label.


  @Regression @TestCase11
  Scenario:Verify Text
    When user clicks on Verify Text link
    Then the User will navigate to text Description page
    And Verify the Welcome Text.
#
  @Regression @TestCase12
  Scenario:Progress Bar
    When user clicks on Progress Bar link
    Then the User will navigate to progress bar Description page
    And Click on the Start button
    And Click on the stop button once the progress reaches "75"%
    Then verify the result and Duration
#
#
#  @Regression @TestCase13
#  Scenario:Visibility
#    When user clicks on Visibility link
#    Then the User will navigate to visibility Description page
#    And Click on the Hide button
#    Then verify that all other buttons are visible or not.
#
#
#  @Regression @TestCase14
#  Scenario:Mouse over
#    When user clicks on Mouse over link
#    Then the User will navigate to MouseOver Description page
#    And Hover the mouse on Click Me button
#    Then verify that text color changes to Orange color
#    And click on click me button twice
#    Then verify that the click count is increased by 2.
#
#
#  @Regression @TestCase15
#  Scenario:Non breaking space
#    When user clicks on Non breaking space link
#    Then the User will navigate to NBSP Description page
#    And find the button with xpath as "//button[text()='My Button']"
#    Then verify that button is not found
#    And find the button with xpath "//button[text()='My&nbsp;Button']"
#    Then verify that button is visible.
#
#
#  @Regression @TestCase16
#  Scenario:Shadow DOM
#    When user clicks on Non ShadowDOM space link
#    Then the User will navigate to ShadowDOM Description page
#    And use click on Gear button and after that click on copy button.
#    Then compare the clip board value with the text in the input field.
#
