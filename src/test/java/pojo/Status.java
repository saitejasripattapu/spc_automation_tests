package pojo;

public enum Status {
    AVAILABLE,
    SOLD,
    PENDING;
}
