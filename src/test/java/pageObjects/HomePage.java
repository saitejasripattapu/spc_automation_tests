package pageObjects;

import Listeners.WebDriverEventHandler;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.GenericUtils;
import utils.TestContextSetup;

import java.time.Duration;
import java.util.List;

public class HomePage {
	public WebDriver driver;
	GenericUtils utils;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		utils = new GenericUtils(driver);

	}

	WebDriverEventHandler webDriverEventHandler= new WebDriverEventHandler();




	By logo = By.xpath(" //a[contains(text(),'UITAP')]]");
	By homeTab = By.xpath("//a[contains(text(),'Home')]");
	By resourcesTab = By.xpath(" //a[contains(text(),'Resources')]");
	By dynamicID = By.xpath("//a[contains(text(),'Dynamic ID')]");
	By dynamicIdButton = By.xpath("//button[@class='btn btn-primary']");
    By dynamicIDHeader = By.xpath("//h3[contains(text(),'Dynamic ID')]");
	By classAttributeHeader = By.xpath("//h3[contains(text(),'Class Attribute')]");
	By AjaxDataHeader = By.xpath("//h3[contains(text(),'AJAX Data')]");
	By clickHeader = By.xpath("//h3[contains(text(),'Click')]");
	By textInputKeyboard = By.xpath("//h3[contains(text(),'Text Input')]");
	By Scenario = By.xpath("//h4[contains(text(),'Scenario')]");

	By classAttribute = By.xpath("//a[contains(text(),'Class Attribute')]");
     By hiddenLayers = By.xpath("//a[contains(text(),'Hidden Layers')]");
	By loadDelay = By.xpath("//a[contains(text(),'Load Delay')]");
	By AjaxData = By.xpath("//a[contains(text(),'AJAX Data')]");
	By clientSideDelay = By.xpath("//a[contains(text(),'Client Side Delay')]");
	By click = By.xpath("//a[contains(text(),'Click')]");
	By textInput = By.xpath("//a[contains(text(),'Text Input')]");
	By scrollBars = By.xpath("//a[contains(text(),'Scrollbars')] ");
	By dynamicTable= By.xpath("//a[contains(text(),'Dynamic Table')]");
	By verifyText= By.xpath("//a[contains(text(),'Verify Text')]");
	By progressBar= By.xpath("//a[contains(text(),'Progress Bar')]");
	By visibility= By.xpath("//a[contains(text(),'Visibility')]");
	By mouseOver= By.xpath("//a[contains(text(),'Mouse Over')]");
	By nonBreakingspace= By.xpath("//a[contains(text(),'Non-Breaking Space')]");
	By overlapLink= By.xpath("//a[contains(text(),'Overlapped Element')]");
	By shadowDOM= By.xpath("//a[contains(text(),'Shadow DOM')]");
	By bluebutton= By.xpath("//button[contains(concat(' ', normalize-space(@class), ' '), ' btn-primary ')]");

	By buttonAfterLoad = By.xpath(("//button[@class='btn btn-primary']"));
	By buttonAfterAjax = By.xpath("//p[contains(text(),'Data loaded with AJAX get request.')]");
	By buttonForMouseClick= By.xpath("//button[@id='badButton']");
	By hidingbutton = By.xpath("//button[@id='hidingButton']");
	By tableheader = By.xpath("//span[@role='columnheader']");
	By chrome= By.xpath("//span[contains(text(),'Chrome')]");
	By cpu= By.xpath("//span[contains(text(),'CPU')]");
	By yellowLabel= By.xpath("//p[@class='bg-warning']");
	By welcomeText= By.xpath("//span[contains(normalize-space(text()),'Welcome')]");
	By startButton = By.xpath("//button[@id='startButton']");
	By stopButton = By.xpath("//button[@id='stopButton']");
	By progresBar = By.xpath("//div[@id='progressBar']");
	By progressResult= By.xpath("//p[@id='result']");

	public void verifyTheTabs() {
		List<WebElement> links = driver.findElements(By.xpath("//a[@class='nav-link']"));
		System.out.println(links.size());
		for (int i = 1; i <= links.size(); i++) {
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
			System.out.print(i + "\t" + links.get(i - 1).getAttribute("innerText") +"\n");
			switch(i)
			{
				case 1: Assert.assertEquals(links.get(i - 1).getAttribute("innerText"), "Home"); break;
				case 2: Assert.assertEquals(links.get(i - 1).getAttribute("innerText"), "Resources"); break;
				default: break;
			}
		}

	}




	public void clickOnDynamicIDLink(){driver.findElement(dynamicID).click();}
	public void clickOnClassAttributeLink(){driver.findElement(classAttribute).click();}
	public void clickOnHiddenLayersLink(){driver.findElement(hiddenLayers).click();}
	public void clickOnLoadDelayLink(){driver.findElement(loadDelay).click();}
	public void clickOnAjaxDataLink(){driver.findElement(AjaxData).click();}
	public void clickOnClientSideDelayLink(){driver.findElement(clientSideDelay).click();}
	public void clickOnClickLink(){driver.findElement(click).click();}
	public void clickOntextInputLink(){driver.findElement(textInput).click();}
	public void clickOnscrollBarsLink(){driver.findElement(scrollBars).click();}
	public void clickOndynamicTableLink(){driver.findElement(dynamicTable).click();}
	public void clickOnverifyTextLink(){driver.findElement(verifyText).click();}
	public void clickOnprogressBarLink(){driver.findElement(progressBar).click();}
	public void clickOnvisibilityLink(){driver.findElement(visibility).click();}
	public void clickOnmouseOverLink(){driver.findElement(mouseOver).click();}
	public void clickOnnonBreakingspaceLink(){driver.findElement(nonBreakingspace).click();}
	public void clickOnshadowDOMLink(){driver.findElement(shadowDOM).click();}


public void clickOnBlueButton()
{
	driver.findElement(bluebutton).click();
}

	public boolean isButtonClickable(){
		return(utils.waitForButtonToBeClickable(dynamicIdButton,5));
	}

	public boolean isButtonVisible(int arg0){
		return(utils.waitForButtonVisibility(buttonAfterAjax,arg0));
	}

	public String isResultDisplayed(){
		return(driver.findElement(progressResult).getText());
	}


   public void clickontheButton(){
		driver.findElement(buttonAfterLoad).click();
   }

	public void clickOnStartButton(){
		driver.findElement(startButton).click();	}



	public void clickOnStopButton() throws InterruptedException {utils.clickOnButton(stopButton);
	Thread.sleep(2000);}

	public void progressBar(String percent) throws InterruptedException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofMillis(18226));
			System.out.println(wait.until(ExpectedConditions.attributeToBe(progresBar, "aria-valuenow", percent)));
		} catch(Exception e) {
				System.out.println("Exception"+e+driver.findElement(progressResult).getText());
		}
	}



   public boolean verifyText(){
		WebElement element=driver.findElement(welcomeText);
		if(element.isDisplayed()){
			System.out.println(element.getAttribute("innerText"));
			return true;
		}

		else
			return false;
   }

	public void clickontheMouseButton(){

		WebElement element = driver.findElement(buttonForMouseClick);
		element.click();
		String bgColor = element.getCssValue("background-color");
		System.out.println("Background color after click " + bgColor);
	}

	public void verifyButtonAfterLoad(){
		utils.isElementPresent(buttonAfterLoad);
		System.out.println(driver.findElement(buttonAfterLoad).getText());

	}

	public void hidingButton() throws InterruptedException {
		driver.findElement(hidingbutton).getText();

	}


		public void scrollToTheElement() throws InterruptedException {
		WebElement element = driver.findElement(hidingbutton);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(500);
		System.out.println(driver.findElement(hidingbutton).getText());
	}

	public boolean verifyTheDynamicIDheader(){return(utils.isElementPresent(dynamicIDHeader));}
	public boolean verifyClassAttributeheader(){return(utils.isElementPresent(classAttributeHeader));}
	public boolean verifyAjaxDataheader(){return(utils.isElementPresent(AjaxDataHeader));}
	public boolean verifyClickHeader(){return(utils.isElementPresent(clickHeader));}
	public boolean verifyTextInputHeader(){return(utils.isElementPresent(textInputKeyboard));}
	public boolean verifyScenarioHeader(){return(utils.isElementPresent(Scenario));}


     public void sendText(String arg0){
		 driver.findElement(By.xpath("//input[@id='newButtonName']")).sendKeys(arg0);
		 WebElement element = driver.findElement(buttonAfterLoad);
		 element.click();	 }

	public void getTextFromButton() throws InterruptedException {
		WebElement element = driver.findElement(buttonAfterLoad);
		Thread.sleep(2000);
		System.out.println(element.getText());
	}

	public void acceptPopUp(){driver.switchTo().alert().accept();}

	public void waitForPageload(int userTime){
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(userTime));
	}

	public void movetoElementbyMouse()
	{
		WebElement element= driver.findElement(buttonForMouseClick);
		Actions builder = new Actions(driver);
		Action mouseOverbutton = builder
				.moveToElement(element)
				.build();
		String bgColor = element.getCssValue("background-color");
		System.out.println("Before hover: " + bgColor);
         mouseOverbutton.perform();
		bgColor = element.getCssValue("background-color");
		System.out.println("After hover: " + bgColor);
	}

   public String getChromeCPUProcess() {
	   List<WebElement> headerList = driver.findElements(tableheader);
	   List<WebElement> rows = driver.findElements(By.xpath("(//div[@role='rowgroup']//div//span[@role='cell'][1])"));
	   System.out.println("Size of header ->" + headerList.size());
	   int headerIndex = -1;
	   int rowIndex = -1;
	   for (int i = 0; i < rows.size(); i++) {
		   boolean elementFound2 = rows.get(i).getText().equalsIgnoreCase("Chrome");
		   if (elementFound2) {
			   System.out.println("Row Index ->" + i);
			   rowIndex = i + 1;
		   }
	   }
	   for (int j = 0; j <headerList.size(); j++) {
		   boolean elementFound = headerList.get(j).getText().equalsIgnoreCase("CPU");
		   if (elementFound) {
			   System.out.println("Column Index->" + j);
			   headerIndex = j + 1;
		   }
	   }
	   String desiredValue=driver.findElement(By.xpath("(//div[@role='rowgroup']//div//span[@role='cell']["+headerIndex+"])["+rowIndex+"]")).getAttribute("innerText");

	   System.out.println("Chrome CPU="+ desiredValue );
	 return desiredValue;
   }

   public String expectedValueforProcess()
   {
	   String expectedValue=driver.findElement(yellowLabel).getAttribute("innerText");
	   return (expectedValue.substring(12));

   }


   public void callingListener(){
      WebElement element= driver.findElement(bluebutton);
	   webDriverEventHandler.beforeClick(element);
	   element.click();
   }
}


