package pageObjects;

import org.openqa.selenium.WebDriver;
import utils.GenericUtils;
import utils.TestContextSetup;

public class PageObjectManager {



	public WebDriver driver;
	public HomePage homePage;
	
	public PageObjectManager(WebDriver driver)
	{
		this.driver = driver;
	}

	
	public HomePage getHomePage()
	{
		homePage = new HomePage(driver);
		return homePage;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}



}
