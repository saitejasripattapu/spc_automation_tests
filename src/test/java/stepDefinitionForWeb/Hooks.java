package stepDefinitionForWeb;


import io.cucumber.java.*;
import org.openqa.selenium.OutputType;
import io.cucumber.java.Scenario;
import org.openqa.selenium.TakesScreenshot;
//import com.aventstack.extentreports.gherkin.model.Scenario;
import utils.TestContextSetup;


public class Hooks {
    TestContextSetup testContextSetup;

    public Hooks(TestContextSetup testContextSetup) {
        this.testContextSetup = testContextSetup;
    }

    @After
    public void AfterScenario(Scenario scenario) throws Exception {
        if(scenario.isFailed())
        {
            byte[] screenshot = ((TakesScreenshot) testContextSetup.testBase.WebDriverManager()).getScreenshotAs(OutputType.BYTES);
            String testName = scenario.getName();
            scenario.attach(screenshot, "image/png",testName);

        }
        testContextSetup.testBase.WebDriverManager().quit();

    }
}