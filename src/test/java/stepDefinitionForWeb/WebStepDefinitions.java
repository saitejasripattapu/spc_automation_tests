package stepDefinitionForWeb;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.testng.Assert;
import pageObjects.HomePage;
import tech.grasshopper.pdf.pojo.cucumber.Scenario;
import utils.TestContextSetup;

public class WebStepDefinitions {

  Scenario scn;
  TestContextSetup testContextSetup;
    public HomePage homePage;
    public String actualValue;

    public WebStepDefinitions(TestContextSetup testContextSetup)
    {
        this.testContextSetup=testContextSetup;
        this.homePage= testContextSetup.pageObjectManager.getHomePage();

    }

    @When("user calls UI Testing playgroundpage")
    public void user_calls_ui_testing_playgroundpage() {

    }
    @Then("the user should successfully navigate to Home Page")
    public void the_user_should_successfully_navigate_to_home_page() {

    }

    @Then("validate that various tabs exists")
    public void validate_that_various_tabs_exists() {
      homePage.verifyTheTabs();
    }


  @When("user clicks on DynamicID link")
  public void userClicksOnDynamicIDLink() {
      homePage.clickOnDynamicIDLink();
  }

  @Then("the User will navigate to Description")
  public void theUserWillNavigateToDescription() {
      homePage.verifyTheDynamicIDheader();
  }

  @And("user will locate the button and will click on it")
  public void userWillLocateTheButtonAndWillClickOnIt() {
    Assert.assertTrue(homePage.isButtonClickable());
  }


  @When("user clicks on ClassAttribute link")
  public void userClicksOnClassAttributeLink() {
      homePage.clickOnClassAttributeLink();
  }

  @Then("the User will navigate to Description page")
  public void theUserWillNavigateToDescriptionPage() {
     Assert.assertTrue(homePage.verifyClassAttributeheader());
  }

  @And("user will locate the blue button and will click on it")
  public void userWillLocateTheBlueButtonAndWillClickOnIt() {
      homePage.clickOnBlueButton();
  }

  @Then("user will click on OK in the pop-up")
  public void userWillClickOnOKInThePopUp() {
      homePage.acceptPopUp();
  }

  @When("user clicks on Load Delay link")
  public void userClicksOnLoadDelayLink() {
    homePage.clickOnLoadDelayLink();
  }

  @Then("the User will navigate to Description page after {int} seconds loading")
  public void theUserWillNavigateToDescriptionPageAfterSecondsLoading(int arg0) {
      homePage.waitForPageload(arg0);
  }

  @And("Verify the button after the page load")
  public void verifyTheButtonAfterThePageLoad() {
    homePage.verifyButtonAfterLoad();
    System.out.println("Button is verified successfully");

  }

  @When("user clicks on Ajax Data  link")
  public void userClicksOnAjaxDataLink() {
      homePage.clickOnAjaxDataLink();
  }

  @And("click on the button and wait for {int} seconds")
  public void clickOnTheButtonAndWaitForSeconds(int arg0) {
      homePage.clickontheButton();
    homePage.isButtonVisible(arg0);

  }

  @Then("verify the text of the loaded label")
  public void verifyTheTextOfTheLoadedLabel() {

  }

  @When("user clicks on Client Side Delay  link")
  public void userClicksOnClientSideDelayLink() {
  }

  @Then("verify the text of the label that appears")
  public void verifyTheTextOfTheLabelThatAppears() {
  }

  @When("user clicks on click link")
  public void userClicksOnClickLink() {
      homePage.clickOnClickLink();
  }

  @And("click on the blue button using mouse event")
  public void clickOnTheBlueButtonUsingMouseEvent() {
      homePage.movetoElementbyMouse();
  }

  @Then("verify the button turns Green color")
  public void verifyTheButtonTurnsGreenColor() {
    homePage.clickontheMouseButton();
  }

  @When("user clicks on Text Input link")
  public void userClicksOnTextInputLink() {
      homePage.clickOntextInputLink();
  }


  @Then("verify the button texts changes to the text entered by you.")
  public void verifyTheButtonTextsChangesToTheTextEnteredByYou() throws InterruptedException {
    homePage.getTextFromButton();
  }

  @When("user clicks on Scroll bars link")
  public void userClicksOnScrollBarsLink() {
      homePage.clickOnscrollBarsLink();
  }

  @And("scroll vertically and horizontally until the button is visible.")
  public void scrollVerticallyAndHorizontallyUntilTheButtonIsVisible() throws InterruptedException {
      homePage.scrollToTheElement();
  }

  @Then("verify the button text is visible.")
  public void verifyTheButtonTextIsVisible() throws InterruptedException {
      homePage.hidingButton();

  }

  @When("user clicks on Dynamic Table link")
  public void userClicksOnDynamicTableLink() {
      homePage.clickOndynamicTableLink();  }

  @And("Get the value of CPU for chrome process.")
  public void getTheValueOfCPUForChromeProcess() {
     actualValue= homePage.getChromeCPUProcess();
  }

  @Then("compare it with the value in the yellow label.")
  public void compareItWithTheValueInTheYellowLabel() {
      Assert.assertEquals(actualValue,homePage.expectedValueforProcess());
  }

  @When("user clicks on Verify Text link")
  public void userClicksOnVerifyTextLink() {
      homePage.clickOnverifyTextLink();
  }

  @And("Verify the Welcome Text.")
  public void verifyTheWelcomeText() {
      Assert.assertTrue(homePage.verifyText());
  }

  @When("user clicks on Progress Bar link")
  public void userClicksOnProgressBarLink() {
      homePage.clickOnprogressBarLink();
  }

  @And("Click on the Start button")
  public void clickOnTheStartButton() {
      homePage.clickOnStartButton();
  }



  @And("Click on the stop button once the progress reaches {string}%")
  public void clickOnTheStopButtonOnceTheProgressReaches(String percent) throws InterruptedException {
      homePage.progressBar(percent);
  }

  @Then("verify the result and Duration")
  public void verifyTheResultAndDuration() {
      homePage.isResultDisplayed();
  }

  @When("user clicks on Visibility link")
  public void userClicksOnVisibilityLink() {
      homePage.clickOnvisibilityLink();
  }

  @And("Click on the Hide button")
  public void clickOnTheHideButton() {
  }

  @Then("verify that all other buttons are visible or not.")
  public void verifyThatAllOtherButtonsAreVisibleOrNot() {
  }

  @When("user clicks on Mouse over link")
  public void userClicksOnMouseOverLink() {
      homePage.clickOnmouseOverLink();
  }

  @And("Hover the mouse on Click Me button")
  public void hoverTheMouseOnClickMeButton() {
  }

  @Then("verify that text color changes to Orange color")
  public void verifyThatTextColorChangesToOrangeColor() {
  }

  @And("click on click me button twice")
  public void clickOnClickMeButtonTwice() {
  }

  @Then("verify that the click count is increased by {int}.")
  public void verifyThatTheClickCountIsIncreasedBy(int arg0) {
  }

  @When("user clicks on Non breaking space link")
  public void userClicksOnNonBreakingSpaceLink() {
      homePage.clickOnnonBreakingspaceLink();
  }

  @And("find the button with xpath as {string}")
  public void findTheButtonWithXpathAs(String arg0) {
  }

  @Then("verify that button is not found")
  public void verifyThatButtonIsNotFound() {
  }

  @And("find the button with xpath {string}")
  public void findTheButtonWithXpath(String arg0) {
  }

  @Then("verify that button is visible.")
  public void verifyThatButtonIsVisible() {
  }

  @And("use click on Gear button and after that click on copy button.")
  public void useClickOnGearButtonAndAfterThatClickOnCopyButton() {
  }

  @Then("compare the clip board value with the text in the input field.")
  public void compareTheClipBoardValueWithTheTextInTheInputField() {
  }

  @When("user clicks on Hiddenlayers link")
  public void userClicksOnHiddenlayersLink() {
  }

  @Then("the User will navigate to hiddenlayers Description page")
  public void theUserWillNavigateToHiddenlayersDescriptionPage() {
    Assert.assertTrue(homePage.verifyScenarioHeader());
  }

  @Then("the User will navigate to Ajax Data Description page")
  public void theUserWillNavigateToAjaxDataDescriptionPage() {
      Assert.assertTrue(homePage.verifyAjaxDataheader());
  }

  @Then("the User will navigate to delay Description page")
  public void theUserWillNavigateToDelayDescriptionPage() {
  }

  @Then("the User will navigate to mouse click Description page")
  public void theUserWillNavigateToMouseClickDescriptionPage() {
      Assert.assertTrue(homePage.verifyClickHeader());
  }

  @Then("the User will navigate to keyboard Description page")
  public void theUserWillNavigateToKeyboardDescriptionPage() {
    Assert.assertTrue(homePage.verifyTextInputHeader());
  }

  @Then("the User will navigate to scroll bars Description page")
  public void theUserWillNavigateToScrollBarsDescriptionPage() {
    Assert.assertTrue(homePage.verifyScenarioHeader());
  }

  @Then("the User will navigate to dynamic table Description page")
  public void theUserWillNavigateToDynamicTableDescriptionPage() {
    Assert.assertTrue(homePage.verifyScenarioHeader());
  }

  @Then("the User will navigate to text Description page")
  public void theUserWillNavigateToTextDescriptionPage() {
      homePage.verifyScenarioHeader();
  }

  @Then("the User will navigate to progress bar Description page")
  public void theUserWillNavigateToProgressBarDescriptionPage() {
    homePage.verifyScenarioHeader();
  }

  @Then("the User will navigate to visibility Description page")
  public void theUserWillNavigateToVisibilityDescriptionPage() {
    homePage.verifyScenarioHeader();
  }

  @Then("the User will navigate to MouseOver Description page")
  public void theUserWillNavigateToMouseOverDescriptionPage() {
    homePage.verifyScenarioHeader();
  }

  @Then("the User will navigate to NBSP Description page")
  public void theUserWillNavigateToNBSPDescriptionPage() {
    homePage.verifyScenarioHeader();
  }

  @When("user clicks on Non ShadowDOM space link")
  public void userClicksOnNonShadowDOMSpaceLink() {
      homePage.clickOnshadowDOMLink();
  }

  @Then("the User will navigate to ShadowDOM Description page")
  public void theUserWillNavigateToShadowDOMDescriptionPage() {
    homePage.verifyScenarioHeader();
  }

  @And("enter the {string} in the text field and click on the button")
  public void enterTheInTheTextFieldAndClickOnTheButton(String arg0) {
      homePage.sendText(arg0);

  }

  @And("user will locate the green button and record click")
  public void userWillLocateTheGreenButtonAndRecordClick() {
      homePage.callingListener();
  }

  @Then("verify that the button cannot be hit twice")
  public void verifyThatTheButtonCannotBeHitTwice() {
  }


}
