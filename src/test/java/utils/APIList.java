package utils;

public enum APIList {
    addPet("/pet"),
    getPetById("/pet"),
    getStoreInventory("/store/inventory"),
    findByStatus("/pet/findByStatus"),
    purchasePet("/store/order"),
    ;

    private String resource;

    APIList(String resource)
    {
        this.resource=resource;
    }

    public String getResource()
    {
        return resource;
    }
}
