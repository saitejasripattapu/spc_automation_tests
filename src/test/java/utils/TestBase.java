package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestBase {

	public WebDriver driver;
	
	public WebDriver WebDriverManager() throws IOException
	{
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"//src//test//resources//cucumber.properties");
		Properties prop = new Properties();
		prop.load(fis);
		String url = prop.getProperty("testURL");
		String browser_properties = prop.getProperty("browser");
		String browser_maven=System.getProperty("browser");
		String browser = browser_maven!=null ? browser_maven : browser_properties;



		if(driver == null)
		{
		if(browser.equalsIgnoreCase("chrome"))
			{
				System.out.println("GET PROP.env--->"+System.getProperty("forkConfig")+"<--");
//				System.out.println(System.getProperties());
				if (System.getProperty("forkConfig") != null && System.getProperty("forkConfig").equalsIgnoreCase("PIPELINE")) {
					System.out.println("Picking up Pipeline Driver: chromedriver_linux");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("start-maximized"); // open Browser in maximized mode
					options.addArguments("disable-infobars"); // disabling infobars
					options.addArguments("--disable-extensions"); // disabling extensions
					options.addArguments("--disable-gpu"); // applicable to windows os only
					options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
					options.addArguments("--no-sandbox"); // Bypass OS security model
					options.addArguments("--headless"); // Headless
					System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver_linux");
					driver = new ChromeDriver(options);
				} else {
					System.out.println("Picking up WINDOWS Driver: chrome_102");
					System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\chromedriver.exe");

					driver = new ChromeDriver();
				}

			}
			if(browser.equalsIgnoreCase("firefox"))
			{
				System.setProperty("webdriver.gecko.driver","//Users//Downloads//geckodriver 5");
				driver = new FirefoxDriver();
			}

			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

		driver.get(url);
		}
		return driver;
	}



}

