package utils;

import java.io.*;
import java.sql.Time;
import java.time.Duration;
import java.util.*;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class GenericUtils {
	public WebDriver driver;

	public GenericUtils(WebDriver driver) {
		this.driver = driver;
	}

	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}

	}

	public boolean waitForButtonVisibility(By by, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(time));
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			System.out.println("Element is located " + element.getText());
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public boolean waitForButtonToBeClickable(By by, int time) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(time));
			WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			element.click();
			System.out.println("Element is located ");
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public WebElement movetoElementByMouse(By by) {
		Actions builder = new Actions(driver);
		WebElement element = driver.findElement(by);
		Action mouseOverbutton = builder
				.moveToElement(element)
				.build();
	return element;
}

	public void clickOnButton(By by){
		driver.findElement(by).click();
	}
  public byte[] getByteScreenshot() throws IOException {
	  File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  byte[] filecontent= FileUtils.readFileToByteArray(src);
	  return filecontent;
  }

  public void dynamicTableData(String rowValue,String columnValue)
  {


  }



}





