# Automation Assignment

### Steps to run the tests locally
- Open your IDE
- Setup your chromedriver in the location: `src/test/resources` with the name: **chromedriver.exe**
- In this project I have used the `chromediver.exe` of version `102` for `Windows` OS and JDK version `8`
- Click on add/edit configurations
- You can select the configuration as shown in the screenshot below
  ![image](/Configurations_popup.png)
- Once you have done the configuration click `Apply` and `Ok`
- Now you can click either `Run` or `Debug` and tests will be running in your local.

### Run the tests pipeline
Pipeline will be automatically triggered when any changes are pushed to git.  
If you wish to manually run the pipeline please follow the below steps.   
1. Click on `CI/CD` menu and select `Pipelines` from the list. 
2. Now on the top right you can see a blue button named `Run pipeline`. Click it 
3. You'll be navigated to a screen with options, no need to change anything in this page, just click on `Run pipeline` button. 
4. Now you can see a new pipeline running

### Test reports - Calliope.pro

Reports are available in this [Calliope.pro link](https://app.calliope.pro/reports/140811)

### About the test scenarios

I have divided the test runners into two suites.
1. Test Runner for APIs
2. Test Runner for Web
